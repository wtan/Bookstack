#!/bin/sh

set -e

composer install
php artisan key:generate --no-interaction 
php artisan migrate --force --no-interaction 

echo "Settings folder permissions for uploads"
chown -R www-data:www-data public/uploads && chmod -R 775 public/uploads
chown -R www-data:www-data storage/uploads && chmod -R 775 storage/uploads

echo "Copying public assets to /var/www/static"
cp -R . /var/www/static

php artisan cache:clear
php artisan view:clear
